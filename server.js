const { Server } = require("socket.io");

const io = new Server({ /* options */ });

io.on("connection", (socket) => {
  console.log("new connection " + socket.id);
  
  socket.on("build", (data) => {
    console.log("build " + data.applicationId);
    io.emit("requestBuild", data)
  })
  
  socket.on("requestBuildProcessed", (data) => {
    console.log("requestBuildProcessed " + data.applicationId);
    io.emit("buildProcessed", data)
  })
  
  socket.on("requestBuildSuccess", (data) => {
    console.log("requestBuildSuccess " + data.applicationId);
    io.emit("buildSuccess", data)
  })
  
  socket.on("requestBuildFailed", (data) => {
    console.log("requestBuildFailed " + data.applicationId);
    io.emit("buildFailed", data)
  })
});

io.listen(3000);