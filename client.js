const { io } = require("socket.io-client");
const socket = io("http://localhost:3000");

socket.on("connect", () => {
  console.log("connect " + socket.id)

  const data = {
    "appName": "Burger King",
    "versionCode": "1",
    "versionName": "1.0",
    "applicationId": "com.burgerking.app",
    "webUrl": "https://bkdelivery.co.id",
    "icon": "logo.png",
    "bgImageSplash": "splash_bg.jpg",
    "bgImageSplashPosition": "full_height",
    "bgImageSplashSnap": "center",
    "bgColorSplash": "#ffffff",
    "logoSplashPosition": "center",
    "splashScreenTime": "2000",
    "splashScreenOrientation": "lock_vertical",
    "homeOrientation": "lock_vertical",
  }

  socket.emit("build", data)
  
  socket.on("buildProcessed", (data) => {
    console.log("buildProcessed " + data.applicationId);
  })
  
  socket.on("buildSuccess", (data) => {
    console.log("buildSuccess " + data.applicationId);
  })
  
  socket.on("buildFailed", (data) => {
    console.log("buildFailed " + data.applicationId);
  })
});