module.exports = {
  apps: [
    {
      name: "instantwebtoapp-socket",
      script: "./server.js",
      env: {
        PORT: 3100,
        NODE_ENV: "production"
      },
      env_production: {
        "NODE_ENV": "production"
      }
    }
  ],
  deploy: {
    production: {
      user: "deploy",
      host: "34.124.149.91",
      key: "./config/key/deploy.pem",
      ref: "origin/production",
      repo: "git@bitbucket.org:instantwebtoapp/instantwebtoapp-socket.git",
      path: "/home/deploy/instantwebtoapp-socket",
      env: { "NODE_ENV": "production" },
      "pre-setup": "rm -rf /home/deploy/instantwebtoapp-socket/source",
      "post-setup": "npm install",
      "post-deploy": "pm2 startOrRestart ecosystem.config.js --env production"
    }
  }
};